# WiFiInfoStat

***

## Name
WiFiInfoStat console application

## Description
The program is designed to understand the quality of wireless communications.
Standard OS tools do not always provide the correct understanding, and third-party programs are usually too multifunctional and redundant.
Insufficient quality of wireless communication causes glitches, lags and other problems in video conferencing and remote work.
The program monitors the network with which the connection is currently established. Monitors in both bands.
For example, the adapter is connected to a network in the 5G band, but the network allows it to work in both 2.4G and 5G.
So, both bands will be monitored.
The repeater network (with the _EXT suffix), if present, is also monitored. Also in 2 bands.

The program collects signal level statistics every 5 seconds in the background. With a storage depth of 15 minutes.
And when you press R (Refresh, Update), it displays both - instantaneous values and averaged statistical values.
The main screen is updated only by pressing R. But in the application title, at the top, brief information is updated every 5 seconds.

If only one network with this name is visible (when broadcasting in both 2.4G and 5G) - the option to use only one of the bands is probably set in the adapter properties.

## Visuals
![Screenshot](Screenshot.png)

## Requirements
Windows OS, with .NET Framework 4.6.1 or greater.

## Authors
Eugene A. Grebenyuk

## License
WiFiInfoStat is Freeware

## Project status
Beta version. Almost completed.
