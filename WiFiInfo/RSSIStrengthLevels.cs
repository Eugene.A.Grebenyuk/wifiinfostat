﻿using System;
using System.Collections.Generic;

namespace WiFiInfo
{
    class RSSIStrengthLevels
    {
        public double rssi;     //Low border of
        public string level;
        public int percent;
        public static List<RSSIStrengthLevels> list;
        public static ConsoleColor colorForLastLevel;

        public RSSIStrengthLevels(double _rssi, string _level, int _percent)
        {
            rssi = _rssi;
            level = _level;
            percent = _percent;
        }

        private static void FillList()
        {
            list = new List<RSSIStrengthLevels>(8);
            list.Add(new RSSIStrengthLevels(-30, "Ideal", 100));
            list.Add(new RSSIStrengthLevels(-50, "Excellent", 100));
            list.Add(new RSSIStrengthLevels(-60, "Good", 90));
            list.Add(new RSSIStrengthLevels(-67, "Medium", 70));         //Reliable
            list.Add(new RSSIStrengthLevels(-70, "Fair", 50));           //Not strong
            list.Add(new RSSIStrengthLevels(-80, "Poor, Unstable", 25)); //Unreliable
            list.Add(new RSSIStrengthLevels(-90, "Very bad", 5));        //Worst
                                                                         //Пределы величины по стандарту: от -10 до -100 dBm
        }

        public static string GetString(double networkRSSI)
        {
            if (list == null)
                FillList();

            if (networkRSSI > -30)
                networkRSSI = -30;

            if (networkRSSI < -100)
                networkRSSI = -100;

            RSSIStrengthLevels previousLevel = list[0];
            RSSIStrengthLevels currentLevel = list[list.Count - 1];
            foreach (RSSIStrengthLevels level in list)
            {
                if (level.rssi <= networkRSSI)
                {
                    currentLevel = level;
                    break;
                }
                else
                    previousLevel = level;
            }

            SetColorForLastLevel(currentLevel);

            int perc = networkRSSI == -30 ? 100 : currentLevel.percent + (int)((networkRSSI - currentLevel.rssi) * (double)(currentLevel.percent - (previousLevel.percent == 5 ? 0 : previousLevel.percent)) / (currentLevel.rssi - (previousLevel.rssi == -90 ? -100 : previousLevel.rssi)));

            return string.Format("{0}   {1}%", currentLevel.level, perc);
        }

        private static void SetColorForLastLevel(RSSIStrengthLevels currentLevel)
        {
            if (currentLevel.percent >= 80)
                colorForLastLevel = ConsoleColor.Green;
            else if (currentLevel.percent >= 60)
                colorForLastLevel = ConsoleColor.DarkYellow;
            else
                colorForLastLevel = ConsoleColor.DarkRed;
        }
    }
    /*
     * -30 dBm  Perfect         100%
     * -50 dBm  Excellent       100%
     * -60 dBm  Good            90%
     * -67 dBm  Medium          70%
     * -70 dBm  Fair            50%
     * -80 dBm  Poor, Unstable  25%
     * -90 dBm  Very bad        5%
     */
}
