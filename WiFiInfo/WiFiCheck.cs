﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading.Tasks;

using Windows.Devices.WiFi;     //1. Add the NuGet package Microsoft.Windows.SDK.Contracts to the project. 2. right-click packages.config, select Migrate packages.config to PackageReference.
//using System.Net;

namespace WiFiInfo
{
    static class WiFiCheck
    {
        private static WiFiAdapter wiFiAdapter;
        private static string wifiDeviceId;
        private static bool networkScasnInProgress = false;
        private const int MAX_GETWIFIADAPTER_COUNT = 10;
        private static int wifiAdapterCallsCountWithoutGC = 0;
        public static event EventHandler<SetConsoleTitleEventArgs> NeedToChangeTitle;
        public static event EventHandler<SetConsoleTextEventArgs> NeedToConsoleWriteErrorLine;
        //private static readonly object locker = new object();

        public static WiFiAdapter GetFirstWiFiAdapter()
        {
            using (Task<WiFiAdapter> t = GetFirstWiFiAdapterAsync())
            {
                t.Wait();
                wiFiAdapter = t.Result;

                return t.Result;
            }
        }

        /// <summary>
        /// Включённый Wi-Fi адаптер. Выключенный не найдёт.
        /// </summary>
        private static async Task<WiFiAdapter> GetFirstWiFiAdapterAsync()
        {
            WiFiAdapter wiFiAdapter = null;
            //WiFiAdapter.RequestAccessAsync()
            Windows.Devices.Enumeration.DeviceInformationCollection wifiDevices;
            wifiDevices = await Windows.Devices.Enumeration.DeviceInformation.FindAllAsync(WiFiAdapter.GetDeviceSelector());
            if (wifiDevices.Count > 0)
            {
                wiFiAdapter = await WiFiAdapter.FromIdAsync(wifiDevices[0].Id);
                wifiDeviceId = wifiDevices[0].Id;
            }
            //или через WiFiAdapter.FindAllAdaptersAsync()
            //wiFiAdapter = (await WiFiAdapter.FindAllAdaptersAsync()).FirstOrDefault();

            return wiFiAdapter;
        }

        /// <summary>Без этого на следующей итерации скорость не обновляется.</summary>
        public static WiFiAdapter RefreshAdapter()
        {
            WiFiAdapter result = null;

            if (wifiDeviceId == null)
                return null;

            try
            {
                using (Task<WiFiAdapter> t = WiFiAdapter.FromIdAsync(wifiDeviceId).AsTask())
                {
                    try
                    {
                        t.Wait();

                        result = t.Result;
                    }
                    catch (AggregateException ex)
                    {
                        //if (!ex.InnerException.Message.Contains("already too many sessions established to that server."))   //Это если язык en-US, а на ru-RU другое.
                        if (ex.InnerException.HResult != -2147023676)
                            throw;
                        //Бывает ошибка при многократном вызове. Кое-где пишут, что это баг. Если словили - просто пропустим одно обновление.
                        //Даже при последовательном обычном вызове бывает, и не редко. И заедает надолго. При этом значение скорости не обновится.
                        result = wiFiAdapter;

                        GC.Collect();                       //Спасение от "already too many sessions established". Но сейчас это здесь страховка, т.к. регулярно ниже GC.Collect() вызываем.
                        wifiAdapterCallsCountWithoutGC = 0;
                    }
                }
            }
            catch (AggregateException ex)
            {
                //if (!ex.InnerException.Message.StartsWith("Element not found."))    //Ru: "Элемент не найден"
                if(ex.InnerException.HResult != -2147023728)
                    throw;
            }

            wifiAdapterCallsCountWithoutGC++;
            if (wifiAdapterCallsCountWithoutGC > MAX_GETWIFIADAPTER_COUNT)
            {
                GC.Collect();
                wifiAdapterCallsCountWithoutGC = 0;
            }

            return result;
        }

        public static void NetworkScan()
        {
            if (wiFiAdapter != null)
            {
                networkScasnInProgress = true;
                try
                {
                    using (Task t = wiFiAdapter.ScanAsync().AsTask())
                        t.Wait();
                }
                catch (AggregateException ex)
                {
                    networkScasnInProgress = false;

                    if (ex.InnerException.Message.Contains("Operation aborted"))
                        NeedToConsoleWriteErrorLine?.Invoke(null, new SetConsoleTextEventArgs("Operation of NetworkScan() aborted."));
                    else
                        throw;
                }
                networkScasnInProgress = false;
            }
        }

        /// <summary>Фоновая проверка каждые 5 сек для сбора статистики беспроводной связи</summary>
        public static void DoRegularCheck(object sender)
        {
            if (networkScasnInProgress)
                return;

            wiFiAdapter = RefreshAdapter();
            //Может wiFiAdapter.ScanAsync() ещё раз в 5 минут вызывать? Иначе потерянные однажды сети не спешит потом отображать. Но ScanAsync() долгий.
            if (wiFiAdapter != null)
            {
                AdapterLinkInfo adapterLinkInfo = QueryAdapterInfo();

                if (adapterLinkInfo.usedNetworkSsid?.Length > 0)
                    adapterLinkInfo.pingTime = PingTime();

                WiFiAdapterStatistics.Add(adapterLinkInfo.inboundMegabitsPerSecond, adapterLinkInfo.outboundMegabitsPerSecond, adapterLinkInfo.linkChannel, adapterLinkInfo.pingTime);

                InfoForTittle forTittle = new InfoForTittle();
                forTittle.rx      = string.Format("RX: {0} Mbps", adapterLinkInfo.inboundMegabitsPerSecond);
                forTittle.channel = string.Format("CH: {0}", adapterLinkInfo.linkChannel);  //Если канал не оптимальный (на основе статистики) - добавить здесь надпись жёлтым "Not optimal"
                forTittle.level   = string.Format("{0}", adapterLinkInfo.signalPercent);

                WiFiAdapterStatistics stat = WiFiAdapterStatistics.GetStatistics(true);
                //WiFiAdapterStatistics medianValues = WiFiAdapterStatistics.GetMedianValue();
                //WiFiAdapterStatistics avgValues = WiFiAdapterStatistics.GetAvgValue();

                NeedToChangeTitle?.Invoke(null, new SetConsoleTitleEventArgs(string.Format("{0} / {1} / {2}   Avg {3:0} Mbps", 
                    forTittle.rx, forTittle.level, forTittle.channel,
                    adapterLinkInfo.usedNetworkSsid != null ? stat.inboundMegabitsPerSecondAvg : 0)));

                if (adapterLinkInfo.usedNetworkSsid?.Length > 0)
                {
                    List<WiFiNetworkInfo> wiFiNetworkInfo = QueryWiFiNetworkInfo(adapterLinkInfo.usedNetworkSsid);
                    WiFiNetworkStatistics.Add(wiFiNetworkInfo);
                }
            }
        }

        public static AdapterLinkInfo QueryAdapterInfo()
        {
            AdapterLinkInfo info = new AdapterLinkInfo();
            NetshWlanShowInterfaceMainInfo wlanMainInfo = NetshWlanCheck.ExecNetshWlanShowInterface();

            info.actualBSSID = wlanMainInfo.actualBSSID;
            info.linkChannel = byte.Parse(wlanMainInfo.channel != null && wlanMainInfo.channel != string.Empty ? wlanMainInfo.channel : "0");
            info.signalPercent = wlanMainInfo.signalPercent;
            info.radioType = wlanMainInfo.radioType;

            if (wiFiAdapter != null)
            {
                info.inboundMegabitsPerSecond = (float)(wiFiAdapter.NetworkAdapter.InboundMaxBitsPerSecond / 1000.0 / 1000.0);
                info.outboundMegabitsPerSecond = (float)(wiFiAdapter.NetworkAdapter.OutboundMaxBitsPerSecond / 1000.0 / 1000.0);
                //wiFiAdapter.NetworkAdapter.NetworkAdapterId
                //wiFiAdapter.NetworkAdapter.NetworkItem.NetworkId

                using (Task<Windows.Networking.Connectivity.ConnectionProfile> tc = wiFiAdapter.NetworkAdapter.GetConnectedProfileAsync().AsTask())
                {
                    tc.Wait();
                    Windows.Networking.Connectivity.ConnectionProfile connectionProfile = tc.Result;
                    if (connectionProfile?.WlanConnectionProfileDetails != null)
                    {
                        info.usedNetworkSsid = connectionProfile.WlanConnectionProfileDetails.GetConnectedSsid();
                        //connectionProfile.GetSignalBars().Value
                        //connectionProfile.ServiceProviderGuid.GetValueOrDefault()     //Пустой
                        //connectionProfile.ProfileName
                        //connectionProfile.GetNetworkConnectivityLevel().ToString()
                    }
                }
            }

            return info;
        }

        public static List<WiFiNetworkInfo> QueryWiFiNetworkInfo(string usedNetworkSsid)
        {
            List<WiFiNetworkInfo> networksList = new List<WiFiNetworkInfo>();

            foreach (WiFiAvailableNetwork network in wiFiAdapter.NetworkReport.AvailableNetworks.Where(a => a.Ssid.StartsWith(usedNetworkSsid.Replace("_EXT", ""))))
            {
                double frequencyMHz = network.ChannelCenterFrequencyInKilohertz / 1000.0;

                WiFiNetworkInfo info = new WiFiNetworkInfo();
                info.ssid    = network.Ssid;
                info.bssid   = network.Bssid;
                info.band    = GetBand(frequencyMHz);
                info.kind    = TranslateKind(network.PhyKind);
                info.channel = byte.Parse(GetChannel(frequencyMHz));
                info.networkRssiInDecibelMilliwatts = network.NetworkRssiInDecibelMilliwatts;
                //network.SecuritySettings.NetworkAuthenticationType.ToString()    //Enum: Wpa3Sae
                //network.SignalBars

                networksList.Add(info);
            }

            return networksList;
        }

        public static long? PingTime()
        {
            string hostToPing = Properties.Settings.Default.HostToPing;
            if (hostToPing != "")
                using (Ping ping = new Ping())
                {
                    PingReply re = null;
                    try
                    {
                        re = ping.Send(hostToPing, Properties.Settings.Default.PingTimeoutMilliseconds);
                    }
                    catch(PingException pex)
                    {
                        //if (pex.InnerException == null || pex.InnerException.Message != "No such host is known")    //Ru: "Этот хост неизвестен"
                        if (pex.InnerException == null || pex.InnerException.HResult != -2147467259)
                            throw;
                    }

                    return re?.Status == IPStatus.Success ? re.RoundtripTime : (long?)null;
                }
            else
                return null;
            
        }

        public static string TranslateKind(WiFiPhyKind kind)
        {
            switch (kind)
            {
                case WiFiPhyKind.HT:
                    return "802.11n";   //Wi-Fi 4

                case WiFiPhyKind.Vht:
                    return "802.11ac";  //Wi-Fi 5

                case WiFiPhyKind.HE:
                    return "802.11ax";  //Wi-Fi 6

                default:
                    return "";
            }
        }

        public static string GetChannel(double frequencyMHz)
        {
            if (frequencyMHz > 2407 && frequencyMHz <= 2485)     //2.4G
                return string.Format("{0}", (frequencyMHz - 2407.0) / 5.0);

            if (frequencyMHz >= 5160 && frequencyMHz <= 5885)   //5G
                return string.Format("{0}", 31 + (frequencyMHz - 5155.0) / 5.0);

            return string.Empty;
        }

        public static string GetBand(double frequencyMHz)
        {
            if (frequencyMHz > 2407 && frequencyMHz <= 2485)
                return "2.4G";

            if (frequencyMHz >= 5160 && frequencyMHz <= 5885)
                return "5G";

            return string.Empty;
        }

        public class AdapterLinkInfo
        {
            public float inboundMegabitsPerSecond;
            public float outboundMegabitsPerSecond;
            public byte linkChannel;    //Может меняться в ходе связи - при автоматическом переключении на другой диапазон
            public string signalPercent;
            public string radioType;
            public string usedNetworkSsid;
            public string actualBSSID;
            public long? pingTime;

            /// <summary>Краткий список параметров - для статистической обработки</summary>
            public AdapterLinkInfo(float inboundMegabitsPerSecond, float outboundMegabitsPerSecond, byte channel, long? pingTime)
            {
                this.inboundMegabitsPerSecond = inboundMegabitsPerSecond;
                this.outboundMegabitsPerSecond = outboundMegabitsPerSecond;
                this.linkChannel = channel;
                this.pingTime = pingTime;
            }

            public AdapterLinkInfo()
            {

            }
        }

        public class WiFiNetworkInfo
        {
            public string ssid;
            public string bssid;
            public string band;
            public string kind;
            public byte channel;
            public double networkRssiInDecibelMilliwatts;
        }
    }
}
