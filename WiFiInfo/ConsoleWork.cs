﻿using System;

namespace WiFiInfo
{
    static class ConsoleWork
    {
        public static void ConsoleWriteLine(string value, ConsoleColor? foregroundColor = null)
        {
            ConsoleWrite(value, foregroundColor);
            Console.WriteLine();
        }

        /*public static void ConsoleWriteLine(object sender, SetConsoleTextEventArgs e)
        {
            Console.WriteLine(e.Text);
        }*/

        public static void ConsoleWriteErrorLine(object sender, SetConsoleTextEventArgs e)
        {
            Console.Error.WriteLine(e.Text);
        }

        public static void ConsoleWrite(string value, ConsoleColor? foregroundColor = null)
        {
            if (foregroundColor.HasValue)
                Console.ForegroundColor = foregroundColor.Value;

            Console.Write(value);

            if (foregroundColor.HasValue)
                Console.ResetColor();
        }

        public static void ConsoleWriteKeyValueLine(string keyWithMask, string value, ConsoleColor foregroundKeyColor, ConsoleColor? foregroundValueColor = null)
        {
            ConsoleColor previousFgColor = Console.ForegroundColor;
            string key = keyWithMask.Split('{')[0];
            string postfix = keyWithMask.Split('}')[1];

            Console.ForegroundColor = foregroundKeyColor;
            Console.Write(key);
            Console.ForegroundColor = foregroundValueColor.HasValue ? foregroundValueColor.Value : previousFgColor;
            Console.Write(value);
            Console.WriteLine(postfix);
            Console.ForegroundColor = previousFgColor;
        }

        public static void TypeLine(string text)
        {
            foreach (char ch in text.ToCharArray())
            {
                Console.Write(ch);
                System.Threading.Thread.Sleep(ch != '.' ? 33 : 200);
            }
        }

        public static void SetConsoleTitle(string title)
        {
            Console.Title = title;
        }

        public static void SetConsoleTitle(object sender, SetConsoleTitleEventArgs e)
        {
            Console.Title = e.Title;
        }
    }

    public class SetConsoleTitleEventArgs : EventArgs
    {
        public string Title { get; set; }

        public SetConsoleTitleEventArgs(string title)
        {
            Title = title;
        }
    }

    public class SetConsoleTextEventArgs : EventArgs
    {
        public string Text { get; set; }

        public SetConsoleTextEventArgs(string text)
        {
            Text = text;
        }
    }
}
