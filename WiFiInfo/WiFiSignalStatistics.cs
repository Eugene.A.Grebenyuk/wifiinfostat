﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WiFiInfo
{
    class WiFiAdapterStatistics
    {
        public float inboundMegabitsPerSecondMin;
        public float inboundMegabitsPerSecondMax;
        public float inboundMegabitsPerSecondAvg;
        public float outboundMegabitsPerSecondMin;
        public float outboundMegabitsPerSecondMax;
        public float outboundMegabitsPerSecondAvg;
        //public float adapterInboundMegabitsPerSecond;
        //public float adapterOutboundMegabitsPerSecond;
        //public byte channel;
        public byte channelChanges;
        public long? pingTimeMin;
        public long? pingTimeMax;
        public long pingPacketsLost;    //В т.ч. по таймаутам
        public float pingPacketLossPercent;
        public float? pingTimeAvg;
        public float? pingTimeStdDev;

        //private static List<WiFiAdapterStatistics> stat = new List<WiFiAdapterStatistics>(180);
        private static List<WiFiCheck.AdapterLinkInfo> stat = new List<WiFiCheck.AdapterLinkInfo>(180);

        /*public WiFiAdapterStatistics(float mbpsIn, float mbpsOut, byte ch, byte channelChangesCount)
        {
            adapterInboundMegabitsPerSecond = mbpsIn;
            adapterOutboundMegabitsPerSecond = mbpsOut;
            channel = ch;
            channelChanges = channelChangesCount;
        }*/

        public static void Add(float inboundMegabitsPerSecond, float outboundMegabitsPerSecond, byte linkChannel, long? pingTime)
        {
            if (stat.Count == 180)
                stat.RemoveAt(0);

            //stat.Add(new WiFiAdapterStatistics(mbpsIn, mbpsOut, ch, 0));
            stat.Add(new WiFiCheck.AdapterLinkInfo(inboundMegabitsPerSecond, outboundMegabitsPerSecond, linkChannel, pingTime));
        }

        public static WiFiAdapterStatistics GetStatistics(bool isShort)
        {
            if (stat.Count > 0)
            {
                WiFiAdapterStatistics result = new WiFiAdapterStatistics();

                result.inboundMegabitsPerSecondAvg = stat.Select(a => a.inboundMegabitsPerSecond).Average();
                if (!isShort)
                {
                    result.inboundMegabitsPerSecondMin = stat.Select(a => a.inboundMegabitsPerSecond).Min();
                    result.inboundMegabitsPerSecondMax = stat.Select(a => a.inboundMegabitsPerSecond).Max();
                    result.outboundMegabitsPerSecondMin = stat.Select(a => a.outboundMegabitsPerSecond).Min();
                    result.outboundMegabitsPerSecondMax = stat.Select(a => a.outboundMegabitsPerSecond).Max();
                    result.outboundMegabitsPerSecondAvg = stat.Select(a => a.outboundMegabitsPerSecond).Average();
                    //Это не точная формула для медианы, точная в случае чётного числа элементов берёт среднее арифметическое от средних соседних. Но здесь это не принципиально.
                    //stat.Select(a => a.adapterInboundMegabitsPerSecond).OrderBy(a => a).ToArray()[stat.Count / 2];
                    //stat.Select(a => a.adapterOutboundMegabitsPerSecond).OrderBy(a => a).ToArray()[stat.Count / 2];
                    //Медианное не очень показательно здесь. Лучше среднее смотреть.
                    result.channelChanges = stat.Select(a => a.linkChannel).CountChanges().First();

                    //IEnumerable<long> pingTimes = stat.Where(a => a.pingTime.HasValue).Select(a => a.pingTime.Value);
                    IEnumerable<long> pingTimes = (from q in stat
                                                   where q.pingTime.HasValue
                                                   select q.pingTime.Value);
                    if (pingTimes.Count() > 0)
                    {
                        result.pingTimeMin = pingTimes.Min();
                        result.pingTimeMax = pingTimes.Max();
                        result.pingTimeAvg = (float)pingTimes.Average();
                        result.pingTimeStdDev = (float)Math.Sqrt(pingTimes.Average(v => Math.Pow(v - result.pingTimeAvg.Value, 2)));
                    }
                    result.pingPacketsLost = Properties.Settings.Default.HostToPing.Length > 0 ? stat.Count - stat.Where(a => a.pingTime.HasValue).Count() : 0;
                    result.pingPacketLossPercent = (float) (100.0 * result.pingPacketsLost / stat.Count);
                }

                return result;
            }
            else
                return null;
        }

        public static byte ItemsCount()
        {
            return (byte) stat.Count;
        }
    }

    public static class EnumerableExtension
    {
        public static IEnumerable<byte> CountChanges(this IEnumerable<byte> source)
        {
            IEnumerator<byte> enumerator = source.GetEnumerator();
            byte changesCount = 0;
            if (enumerator.MoveNext())
            {
                byte previous = enumerator.Current;

                while (enumerator.MoveNext())
                    if (enumerator.Current != previous)
                    {
                        changesCount++;
                        previous = enumerator.Current;
                    }
            }
            yield return changesCount;
        }
    }

    class WiFiNetworkStatistics
    {
        public string bssid;
        public double networkRssiInDecibelMilliwatts;

        /// <summary>Сеть может быть найдена не сразу, и не каждый опрос даст её параметры. Поэтому при каждом опросе разное количество сетей, списком.</summary>
        private static List<List<WiFiNetworkStatistics>> stat = new List<List<WiFiNetworkStatistics>>(180);

        public WiFiNetworkStatistics(string bssid, double networkRssiInDecibelMilliwatts)
        {
            this.bssid = bssid;
            this.networkRssiInDecibelMilliwatts = networkRssiInDecibelMilliwatts;
        }

        public static void Add(/*string bSSID, double rssiInDecibelMilliwatts*/List<WiFiCheck.WiFiNetworkInfo> wiFiNetworskInfo)
        {
            if (stat.Count == 180)
                stat.RemoveAt(0);

            //WiFiSignalStatistics.ItemsCount()
            /*List<WiFiNetworkStatistics> listof = new List<WiFiNetworkStatistics>();
            foreach(WiFiCheck.WiFiNetworkInfo info in wiFiNetworskInfo)
                listof.Add(new WiFiNetworkStatistics(info.bssid, info.networkRssiInDecibelMilliwatts));*/

            stat.Add(wiFiNetworskInfo.Select(a => new WiFiNetworkStatistics(a.bssid, a.networkRssiInDecibelMilliwatts)).ToList());
            //stat.Add(new WiFiNetworkStatistics(wiFiNetworskInfo.Select(a => a.bssid) bSSID, rssiInDecibelMilliwatts));
        }

        public static double? GetRSSIAvgValue(string bssid)
        {
            if (stat.Count > 0)
            {
                List<WiFiNetworkStatistics> oneWiFiNetStat = stat.Select(networks => networks.Find(network => network.bssid == bssid)).ToList();

                double rssiInDecibelMilliwattsAvg = oneWiFiNetStat.Select(a => a?.networkRssiInDecibelMilliwatts ?? -100.0).Average();
                //Те опросы, где указанная сеть была не найдена, считаются по минимуму: за -100 dBm.

                return rssiInDecibelMilliwattsAvg;
            }
            else
                return null;
        }
        //TODO: можно объединить эти 2 метода в один "GetRSSIStatistics", переделав поля класса, и добавив использование WiFiNetworkInfo

        public static double? GetRSSIStdDev(string bssid)
        {
            if (stat.Count > 0)
            {
                List<WiFiNetworkStatistics> oneWiFiNetStat = stat.Select(networks => networks.Find(network => network.bssid == bssid)).ToList();
                double avg = GetRSSIAvgValue(bssid).Value;
                double rssiInDecibelMilliwattsStdDev = Math.Sqrt(oneWiFiNetStat.Select(a => a?.networkRssiInDecibelMilliwatts ?? -100.0).Average(v => Math.Pow(v - avg, 2)));

                return rssiInDecibelMilliwattsStdDev;
            }
            else
                return null;
        }
    }

    class InfoForTittle
    {
        public string rx;
        public string level;
        public string channel;
    }
}
