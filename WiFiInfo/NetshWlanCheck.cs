﻿using System;

namespace WiFiInfo
{
    class NetshWlanCheck
    {
        /// <summary>
        /// <para>Без вызова netsh wlan не обошлось. Требуется прежде всего чтобы узнать BSSID активной сети.</para>
        /// <para>Сеть с одним именем (SSID) может быть в диапазонах и 2.4G и 5G одновременно. Но у них разный BSSID.</para>
        /// <para>По BSSID найдём активный диапазон. Он может меняться в ходе работы!</para>
        /// </summary>
        public static NetshWlanShowInterfaceMainInfo ExecNetshWlanShowInterface()
        {
            string outStuff;
            using (System.Diagnostics.Process proc = new System.Diagnostics.Process())
            {
                proc.StartInfo.FileName = "netsh.exe";
                proc.StartInfo.Arguments = "wlan show interface";
                proc.StartInfo.UseShellExecute = false;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.Start();
                proc.WaitForExit();
                outStuff = proc.StandardOutput.ReadToEnd();
            }

            NetshWlanShowInterfaceMainInfo wlanMainInfo = new NetshWlanShowInterfaceMainInfo();

            switch(System.Threading.Thread.CurrentThread.CurrentUICulture.ThreeLetterISOLanguageName)
            {
                //System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-US") на язык вывода netsh wlan не влияет, поэтому так
                case "rus":
                    if (outStuff.Contains("В системе 1 интерфейс"))
                    {
                        string[] splittedOut = outStuff.Split(Environment.NewLine.ToCharArray());
                        wlanMainInfo.actualBSSID = GetValueFromString("BSSID", splittedOut);
                        wlanMainInfo.radioType = GetValueFromString("Тип радио", splittedOut);
                        wlanMainInfo.channel = GetValueFromString("Канал", splittedOut);
                        wlanMainInfo.signalPercent = GetValueFromString("Сигнал", splittedOut);
                    }
                    break;

                case "eng":

                default:
                    if (outStuff.Contains("There is 1 interface on the system"))  //": disconnected"
                    {
                        string[] splittedOut = outStuff.Split(Environment.NewLine.ToCharArray());
                        wlanMainInfo.actualBSSID = GetValueFromString("BSSID", splittedOut);
                        wlanMainInfo.radioType = GetValueFromString("Radio type", splittedOut);
                        wlanMainInfo.channel = GetValueFromString("Channel", splittedOut);
                        wlanMainInfo.signalPercent = GetValueFromString("Signal", splittedOut);
                    }
                    break;
            }

            return wlanMainInfo;
        }

        private static string GetValueFromString(string paramName, string[] splittedOut)
        {
            foreach (string s in splittedOut)
                if (s.TrimStart().StartsWith(paramName))
                    return s.Substring(s.IndexOf(": ") + 2).Trim();

            return String.Empty;
        }
    }

    class NetshWlanShowInterfaceMainInfo
    {
        public string actualBSSID;
        public string radioType;
        public string channel;
        public string signalPercent;
    }
}
