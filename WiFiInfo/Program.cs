﻿using System;
using System.Collections.Generic;
using System.Threading;
using Windows.Devices.WiFi;     //1. Add the NuGet package Microsoft.Windows.SDK.Contracts to the project. 2. right-click packages.config, select Migrate packages.config to PackageReference.

namespace WiFiInfo
{
    class Program
    {
        /// <summary>Фоновая проверка каждые 5 сек для сбора статистики беспроводной связи</summary>
        private static Timer regularCheck = new Timer(WiFiCheck.DoRegularCheck, null, 
            Properties.Settings.Default.BackgroundCheckIntervalSeconds * 1000, 
            Properties.Settings.Default.BackgroundCheckIntervalSeconds * 1000);

        static void Main(string[] args)
        {
            WiFiAdapter wiFiAdapter = WiFiCheck.GetFirstWiFiAdapter();
            WiFiCheck.NeedToChangeTitle += ConsoleWork.SetConsoleTitle;
            WiFiCheck.NeedToConsoleWriteErrorLine += ConsoleWork.ConsoleWriteErrorLine;
            bool isFirstCheck = true;

            if (wiFiAdapter != null)
            {
                ConsoleKey pressedKey;
                do
                {
                    WiFiCheck.AdapterLinkInfo adapterLinkInfo = WiFiCheck.QueryAdapterInfo();
                    WiFiAdapterStatistics stat = WiFiAdapterStatistics.GetStatistics(false);
                    InfoForTittle forTittle = new InfoForTittle();

                    Console.Clear();
                    //wiFiAdapter = WiFiCheck.RefreshAdapter();   //Без этого на следующей итерации скорость не обновляется. А ScanAsync() долгий.
                    Console.Write(string.Format("Link speed, Receive:  {0} Mbps", adapterLinkInfo.inboundMegabitsPerSecond).PadRight(46));
                    if (stat != null)
                        ConsoleWork.ConsoleWrite(string.Format(" [{0:0} - {1:0}], Avg {2:0} Mbps", 
                            stat.inboundMegabitsPerSecondMin, 
                            stat.inboundMegabitsPerSecondMax, 
                            stat.inboundMegabitsPerSecondAvg), ConsoleColor.DarkGray);
                    Console.WriteLine();
                    Console.Write(string.Format("Link speed, Transmit: {0} Mbps", adapterLinkInfo.outboundMegabitsPerSecond).PadRight(46));
                    if (stat != null)
                        ConsoleWork.ConsoleWrite(string.Format(" [{0:0} - {1:0}], Avg {2:0} Mbps", 
                            stat.outboundMegabitsPerSecondMin, 
                            stat.outboundMegabitsPerSecondMax, 
                            stat.outboundMegabitsPerSecondAvg), ConsoleColor.DarkGray);
                    Console.WriteLine();
                    ConsoleWork.ConsoleWriteKeyValueLine("Active standard: {0}", adapterLinkInfo.radioType, ConsoleColor.DarkGray);
                    ConsoleWork.ConsoleWriteKeyValueLine("Signal: {0}", adapterLinkInfo.signalPercent, ConsoleColor.DarkGray);
                    Console.WriteLine(stat == null || /*(stat != null &&*/ stat.channelChanges == 0 ? string.Format("Channel: {0}", adapterLinkInfo.linkChannel) : string.Format("Channel: {0}    (changed {1} times)", adapterLinkInfo.linkChannel, stat.channelChanges));

                    forTittle.rx = string.Format("RX: {0} Mbps", adapterLinkInfo.inboundMegabitsPerSecond);
                    forTittle.channel = string.Format("CH: {0}", adapterLinkInfo.linkChannel);  //Если канал не оптимальный (на основе статистики) - добавить здесь надпись жёлтым "Not optimal"
                    forTittle.level = string.Format("{0}", adapterLinkInfo.signalPercent);
                    ConsoleWork.SetConsoleTitle(string.Format("{0} / {1} / {2}", forTittle.rx, forTittle.level, forTittle.channel));

                    if (adapterLinkInfo.usedNetworkSsid?.Length > 0)
                    {
                        if (Properties.Settings.Default.HostToPing != "")
                        {
                            long? pingTime = WiFiCheck.PingTime();
                            Console.Write(string.Format("Ping time: {0} ms", pingTime.HasValue ? pingTime.Value.ToString() : "-").PadRight(46));
                            if (stat != null && stat.pingTimeAvg.HasValue)
                                ConsoleWork.ConsoleWrite(string.Format(" [{0:0} - {1:0}], Avg {2:0} ms, SD {3:0}  {4} [{5}]",
                                    stat.pingTimeMin.Value,
                                    stat.pingTimeMax.Value,
                                    stat.pingTimeAvg.Value,
                                    stat.pingTimeStdDev.Value,
                                    stat.pingPacketsLost > 0 ? string.Format(" LOST: {0} ", stat.pingPacketsLost) : string.Empty,
                                    Properties.Settings.Default.HostToPing), ConsoleColor.DarkGray);
                            Console.WriteLine();
                        }
                        Console.WriteLine("SSID: {0}", adapterLinkInfo.usedNetworkSsid);

                        List<WiFiCheck.WiFiNetworkInfo> wiFiNetworskInfo = WiFiCheck.QueryWiFiNetworkInfo(adapterLinkInfo.usedNetworkSsid);

                        foreach(WiFiCheck.WiFiNetworkInfo wiFiNetworkInfo in wiFiNetworskInfo)
                        {
                            Console.WriteLine();
                            Console.Write(string.Format("{0}", wiFiNetworkInfo.ssid).PadRight(17));
                            Console.Write(string.Format("{0} | {1} | CH: {2}     ", wiFiNetworkInfo.band.PadLeft(4), wiFiNetworkInfo.kind.PadRight(8), wiFiNetworkInfo.channel.ToString().PadRight(3)));
                            ConsoleWork.ConsoleWriteLine(string.Format("{0}{1}", wiFiNetworkInfo.bssid, wiFiNetworkInfo.bssid == adapterLinkInfo.actualBSSID ? "     (current)" : ""), ConsoleColor.DarkGray);

                            ConsoleColor previousFgColor = Console.ForegroundColor;
                            Console.Write("RSSI Now: {0} dBm ", wiFiNetworkInfo.networkRssiInDecibelMilliwatts);
                            Console.Write("    | ");
                            string levelGrade = RSSIStrengthLevels.GetString(wiFiNetworkInfo.networkRssiInDecibelMilliwatts);
                            Console.ForegroundColor = RSSIStrengthLevels.colorForLastLevel;
                            Console.WriteLine(levelGrade);
                            Console.ForegroundColor = previousFgColor;

                            double? rssiAvg = WiFiNetworkStatistics.GetRSSIAvgValue(wiFiNetworkInfo.bssid);
                            double? rssiStdDev = WiFiNetworkStatistics.GetRSSIStdDev(wiFiNetworkInfo.bssid);
                            if (rssiAvg.HasValue)
                                Console.WriteLine(string.Format("RSSI Avg: {0:0} dBm     | {1} | SD: {2:0}", rssiAvg.Value, RSSIStrengthLevels.GetString(rssiAvg.Value), rssiStdDev.Value));
                        }
                    }
                    Console.WriteLine();
                    Console.WriteLine();

                    if (isFirstCheck)
                    {
                        ConsoleWork.TypeLine("Done. Press any key. Or press R to Refresh, H to show Help.");
                        isFirstCheck = false;

                        if (Properties.Settings.Default.HostToPing.Length > 0 
                            && Properties.Settings.Default.PingTimeoutMilliseconds >= 1000 * Properties.Settings.Default.BackgroundCheckIntervalSeconds)
                            Console.WriteLine("WARNING: The ping timeout exceeds background check interval! Check .config file!");
                    }
                    else
                        Console.WriteLine("Done. Press any key. Or press R to Refresh, H to show Help.");

                    ConsoleKeyInfo keyInfo = Console.ReadKey();
                    pressedKey = keyInfo.Key;

                    if (pressedKey == ConsoleKey.H)
                        ShowHelp();

                    wiFiAdapter = WiFiCheck.RefreshAdapter();   //Без этого на следующей итерации скорость не обновляется.

                    if (wiFiAdapter == null)
                    {
                        //Если в ходе работы адаптер выключили.
                        Console.WriteLine("{0}There is no WiFi adapters.{0}{0}Done. Press any key.", Environment.NewLine);
                        Console.ReadKey();
                        break;
                    }
                    else if(keyInfo.Modifiers == ConsoleModifiers.Shift && pressedKey == ConsoleKey.R)
                    {
                        Console.WriteLine("\r\nPlease wait..");
                        WiFiCheck.NetworkScan();
                    }
                }
                while (pressedKey == ConsoleKey.R || pressedKey == ConsoleKey.H);
            }
            else
            {
                Console.WriteLine("There is no WiFi adapters.\r\n\r\nDone. Press any key.");
                Console.ReadKey();
            }

            /*if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                System.Net.NetworkInformation.NetworkInterface niWiFi = System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces().First(a => a.NetworkInterfaceType == System.Net.NetworkInformation.NetworkInterfaceType.Wireless80211 && a.OperationalStatus == System.Net.NetworkInformation.OperationalStatus.Up);
                //Console.WriteLine(niWiFi.);
            }*/
            regularCheck.Dispose();
        }

        private static void ShowHelp()
        {
            Console.Clear();
            ConsoleWork.ConsoleWriteLine(Properties.Resources.HelpText, ConsoleColor.DarkGreen);
            Console.WriteLine();
            Console.WriteLine("Press any key.");
            Console.ReadKey();
        }
    }
}
